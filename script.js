var a = 10;
var b = 34;
var c = 4;

var name = "Bonżur Ania! ";
alert(name + b);

function myFunction(a, b) {
    var result = a + b;
    return result;
}

var x = myFunction(2, 12);
alert(x);
var y = myFunction(8, 9);
alert(y);

// dynamicze zamienianie sekcji "top"
window.onload = function() {
    var top = document.getElementById('top');
    top.innerHTML = '<h2> Nowa zawartość Top </h2>';
    top.style.color = 'pink';

    var paragraphs = document.getElementsByTagName('p');
    console.log("Na stronie znajduję ' + paragraphs.length + ' akapity.");
}

window.onload = function() {
    var buttons = document.getElementsById('button');
    buttons.onclick = function() {
        alert('Kliknięcie na przycisku');
    }
}