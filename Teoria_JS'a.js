// Tworzenie wyskakujących alertów 
alert("Witaj!");

// -------------- ZMIENNE W JS'E -------------------
// typy wartości
var a = 1;

var name = "Jan"

var selected = true;

// ----------------- FUNKCJE -------------------------
function myFunction(a, b) {
    var result = a + b;
    return result;
}

var x = myFunction(2, 12);
alert(x);
// ----------------- INSTRUKCJE WARUNKOWE --------------
var price = 230;

if (price >= 200) {
    alert("Cena jest wyższa niż 200 zł - przysługuje Ci rabat");
} else {
    alert("Cena nie przekraca 200 zł - nie przysługuje Ci rabat")
}

// ----------------- ITERACJA ---------------------------
var i = 1;
while (1 <= 10) {
    alert(i);
    i++;
}

for (var i = 1; i <= 10; i++) {
    alert(i);
}
//---------------- TABLICE / KOLEKCJE ----------------------
var numbers = [1, 4, 23, 64, 87, 90];
var myArray = [56, "test", true, 90];

alert(myArray); //   <--- wyświetla całą zawartość tablicy
alert(myArray[1]); // <--- wyświetla 2 wartość z tablicy
//iteracja tablicy
for (var i = 0; i < myArray.length; i++) {
    alert(myArray[1]);
}
// -------------- String - wprowadzenie ---------------------
var string = "To jest \nprzykładowy String"; // <- "\n" to new line i wymuszenie przejścia do nowej linii
alert(string + ", a to jego kontynuacja"); // <- łaczenie różnych ciągów w dłuższy wynikowy tekst 
// nosi nazwę "konkatynacja"
var a = 1;
var b = '1';
alert(a + b); // przykład konkatynacji- rezultat '11'
var c = 1;
alert(a + c); // rezultat '2';

// ------------------ OBIEKTY ------------------------
var age = null;
var person = {
    name: 'Jan Kowalski',
    age: 34,
    emai: 'jkowalski@buziaczek.pl',
    sayHello: function() {
        alert('Witaj ' + this.name);
    }
}

person.sayHello;

//
// 1. Obiekty wbudowane
// - Number
// - Math
// - String
// - Date
// - RegExp (wyrażenie regularne)
var today = new Date(); // nowy obiekt typu daty
today.getDay(); // Jaki jest dzień tygodnia (od 0 do 6, gdzie 0 to niedziela)
today.getMonth(); // Jaki jest miesiąc (od 0 do 11)

// -------------- Document Object Model (DOM) ----------------------------------
// - Obiektowy model dokumenty
// - Programistyczny interfejs dla dokumentów HTML oraz XML
// - DOM umożliwia manipulowanie zawartościądokumentu w sposób programistyczny
// - DOM nie jest częścią języka JavaScript
//
// METODY ORAZ WŁAŚCIWOŚCI
var top = document.getElementById("top");
top.innerHTML = "Dynamiczna zawartość";

//np. doklejenie nowego nagłówka
document.write('<h1>Nowa treść</h1>');

// dynamicze uzupełnianie sekcji "top"

// -------------- ZDARZENIA JAVASCRIPT -------------------------------------------------
// window.onload= function() {}  <-------- funkcja anonimow
// - onscroll
// - onload
// - onclick
// - onresize